import React, {Component} from 'react';
import './miniCalculator.less';

class MiniCalculator extends Component {

  constructor(props) {
    super(props);
    this.state = {
      sum: 0
    }
  }

  add = () => {
    this.setState({sum: this.state.sum + 1});
  };

  subtract = () => {
    this.setState({sum: this.state.sum - 1})
  };
  multiply = () => {
    this.setState({sum: this.state.sum * 2})
  };
  divide = () => {
    this.setState({sum: this.state.sum / 2})
  };

  render() {
    const {sum} = this.state;
    const {add,subtract,multiply,divide}=this;
    return (
      <section>
        <div>计算结果为:
          <span className="result"> {sum} </span>
        </div>
        <div className="operations">
          <button onClick={add}>加1</button>
          <button onClick={subtract}>减1</button>
          <button onClick={multiply}>乘以2</button>
          <button onClick={divide}>除以2</button>
        </div>
      </section>
    );
  }
}

export default MiniCalculator;

